
/*
 * EventOS Log Module
 * Copyright (c) 2022, EventOS Team, <event-os@outlook.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * https://www.event-os.cn
 * https://github.com/event-os/eventos
 * https://gitee.com/event-os/eventos
 * 
 * Change Logs:
 * Date           Author        Notes
 * 2022-08-31     Dog           V0.1
 */

#ifndef __ELOG_TEST_H__
#define __ELOG_TEST_H__

/* include ------------------------------------------------------------------ */
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/* public function ---------------------------------------------------------- */
void elog_test_start(void);

#ifdef __cplusplus
}
#endif

#endif /* __ELOG_TEST_H__ */

/* ----------------------------- end of file -------------------------------- */